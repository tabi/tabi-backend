Tabi Backend
============
[![pipeline status](https://gitlab.com/tabi/tabi-backend/badges/master/pipeline.svg)](https://gitlab.com/tabi/tabi-backend/commits/master)


This is where the collected data from the Tabi App is sent to. The backend is written in [Go](https://golang.org/doc/) and deployed with [Docker](https://www.docker.com/what-docker). Data is stored in a [PostgreSQL](https://www.postgresql.org/docs/) database.

Installation
============
## Binaries


### Latest downloads
See the links below for the latest downloads.

- [tabi-backend darwin-amd64 (Mac OS X)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-backend-darwin-amd64?job=compile)
- [tabi-backend linux/386 (Linux 32-bit)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-backend-linux-386?job=compile)
- [tabi-backendlinux/amd64 (Linux 64-bit)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-backend-linux-amd64?job=compile)
- [tabi-backend windows/386 (Windows 32-bit)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-backend-windows-386.exe?job=compile)
- [tabi-backend windows/amd64 (Windows 64-bit)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-backend-windows-amd64.exe?job=compile)
- [tabi-util darwin-amd64 (Mac OS X)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-util-darwin-amd64?job=compile)
- [tabi-util linux/386 (Linux 32-bit)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-util-linux-386?job=compile)
- [tabi-util linux/amd64 (Linux 64-bit)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-util-linux-amd64?job=compile)
- [tabi-util windows/386 (Windows 32-bit)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-util-windows-386.exe?job=compile)
- [tabi-util windows/amd64 (Windows 64-bit)](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/raw/tabi-util-windows-amd64.exe?job=compile)
- [All binaries](https://gitlab.com/tabi/tabi-backend/-/jobs/artifacts/master/download?job=compile)

### Configuration
- Download a binary.
- Add a `config.toml` file:

  ```
  secretkey = "jcURHvfGDjG6doGeqiVGKJ8qPB7XiAb7zGDJBhWB4Hpgh7GgrhmYmLg"
  host = ":8000"
  loglevel = "info"
  dbtype = "postgres"


  [postgres]
  host = "localhost"
  user = "postgres"
  database = "postgres"
  password = "postgres"
  ```
  Change the values according to your environment.
- Run the executable on Linux/Mac
```
./tabi-backend-(platform)-(architecture)
```
