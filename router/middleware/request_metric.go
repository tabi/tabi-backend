package middleware

import (
	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/tabi/tabi-backend/server"
	"gitlab.com/vpl/httproutermiddleware"
	"net/http"
	"strconv"
)

type requestMetricResponseWriter struct {
	http.ResponseWriter
	httpRequestsMetric         *prometheus.CounterVec
	durationHttpRequestsMetric prometheus.Summary
	statusSet                  bool
}

//  If WriteHeader has not yet been called, Write calls
//  WriteHeader(http.StatusOK) before writing the data.
func (w requestMetricResponseWriter) Write(b []byte) (int, error) {
	if !w.statusSet {
		w.httpRequestsMetric.WithLabelValues("200").Inc()
	}

	return w.ResponseWriter.Write(b)
}

func (w requestMetricResponseWriter) WriteHeader(status int) {
	w.statusSet = true
	w.httpRequestsMetric.WithLabelValues(strconv.Itoa(status)).Inc()
	w.ResponseWriter.WriteHeader(status)
}

func RequestMetricMiddlewareFunc(metrics server.Metrics) httproutermiddleware.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
		timer := prometheus.NewTimer(metrics.HttpRequestsDuration)
		defer timer.ObserveDuration()

		rrw := requestMetricResponseWriter{ResponseWriter: rw, httpRequestsMetric: metrics.HttpRequestsTotal, durationHttpRequestsMetric: metrics.HttpRequestsDuration}
		next(rrw, r, ps)
	}
}
