package middleware

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/util"
	"net/http"
)

func NewClientAuthenticationMiddleware(getClient GetClientFunc, unauthorized UnauthorizedClientFunc) *ClientAuthenticationMiddleware {
	return &ClientAuthenticationMiddleware{getClient: getClient, unauthorizedClient: unauthorized}
}

type GetClientFunc func(string) (*model.Client, error)

type UnauthorizedClientFunc func(r *http.Request, clientId string, clientKey string)

type ClientAuthenticationMiddleware struct {
	getClient          GetClientFunc
	unauthorizedClient UnauthorizedClientFunc
}

func (c *ClientAuthenticationMiddleware) ServeHandle(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
	clientId, clientKey := util.GetClientInfoFromRequest(r)

	client, err := c.getClient(clientId)

	if err != nil || client.Key != clientKey || !client.Allowed {
		rw.Header().Set("Content-Type", "application/json")
		rw.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(rw, "\"Unauthorized client\"")

		if c.unauthorizedClient != nil {
			c.unauthorizedClient(r, clientId, clientKey)
		}

		return
	}

	next(rw, r, ps)
}
