package middleware

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func NewBasicAuthenticationMiddleware(username string, password string) *BasicAuthenticationMiddleware {
	mid := &BasicAuthenticationMiddleware{}
	mid.AddUser(username, password)

	return mid
}

type BasicAuthenticationMiddleware struct {
	Users []BasicAuthUser
}

func (b *BasicAuthenticationMiddleware) AddUser(username string, password string) {
	u := BasicAuthUser{Username: username, Password: password}
	b.Users = append(b.Users, u)
}

func (b *BasicAuthenticationMiddleware) validateUser(username string, password string) bool {
	for _, v := range b.Users {
		if v.Username == username && v.Password == password {
			return true
		}
	}
	return false
}

type BasicAuthUser struct {
	Username string
	Password string
}

func (c *BasicAuthenticationMiddleware) ServeHandle(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
	rw.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)

	u, p, ok := r.BasicAuth()

	if !ok || !c.validateUser(u, p) {
		rw.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(rw, "Unauthorized metrics client")

		return
	}

	next(rw, r, ps)
}
