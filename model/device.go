package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
)

// swagger:model
type Device struct {
	// readOnly: true
	ID uint
	// readOnly: true
	CreatedAt null.NullTime `db:"created_at"`
	// readOnly: true
	UpdatedAt null.NullTime `db:"updated_at"`
	// readOnly: true
	DeletedAt null.NullTime `db:"deleted_at"`
	// readOnly: true
	UserId                 uint            `db:"user_id"`
	Manufacturer           null.NullString `json:",omitempty"`
	Model                  null.NullString `json:",omitempty"`
	OperatingSystem        null.NullString `db:"os"`
	OperatingSystemVersion null.NullString `db:"os_version"`
}
