package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
	"time"
)

type UserStop struct {
	Id        uint
	DeviceId  uint `db:"device_id"`
	LocalId   uint `db:"local_id"`
	Name      null.NullString
	Latitude  float64
	Longitude float64
	Timestamp null.NullTime
	CreatedAt null.NullTime `db:"created_at"`
	UpdatedAt null.NullTime `db:"updated_at"`
	DeletedAt null.NullTime `db:"deleted_at"`
}

type UserStopVisit struct {
	Id             uint
	DeviceId       uint           `db:"device_id"`
	LocalId        uint           `db:"local_id"`
	StopId         null.NullInt64 `db:"stop_id"`
	LocalStopId    uint           `db:"local_stop_id"`
	BeginTimestamp time.Time      `db:"begin_timestamp"`
	EndTimestamp   time.Time      `db:"end_timestamp"`
	CreatedAt      null.NullTime  `db:"created_at"`
	UpdatedAt      null.NullTime  `db:"updated_at"`
	DeletedAt      null.NullTime  `db:"deleted_at"`
}

type UserStopMotive struct {
	Id               uint
	DeviceId         uint           `db:"device_id"`
	StopVisitId      null.NullInt64 `db:"stop_visit_id"`
	LocalStopVisitId uint           `db:"local_stop_visit_id"`
	Motive           string         `db:"motive"`
	Timestamp        null.NullTime  `db:"timestamp"`
	CreatedAt        null.NullTime  `db:"created_at"`
	UpdatedAt        null.NullTime  `db:"updated_at"`
	DeletedAt        null.NullTime  `db:"deleted_at"`
}
