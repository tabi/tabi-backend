package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
)

type TransportationMode struct {
	ID           uint
	TrackId      null.NullInt64 `db:"track_id"`
	LocalTrackId null.NullInt64 `db:"local_track_id"`
	DeviceId     null.NullInt64 `db:"device_id"`

	Timestamp null.NullTime

	ActiveModes []string

	CreatedAt null.NullTime `db:"created_at"`
	UpdatedAt null.NullTime `db:"updated_at"`
	DeletedAt null.NullTime `db:"deleted_at"`
}
