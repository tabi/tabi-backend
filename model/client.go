package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
)

type Client struct {
	Id        string
	Key       string
	Allowed   bool
	CreatedAt null.NullTime `db:"created_at" json:",omitempty"`
	UpdatedAt null.NullTime `db:"updated_at" json:",omitempty"`
	DeletedAt null.NullTime `db:"deleted_at" json:",omitempty"`
}
