package model

import (
	"github.com/jmoiron/sqlx/types"
	"gitlab.com/tabi/tabi-backend/model/null"
)

type UserDeviceConfig struct {
	// readOnly: true
	ID     uint
	Config types.JSONText `json:"config,omitempty"`
	UserId uint           `db:"user_id"`
	// readOnly: true
	CreatedAt null.NullTime `db:"created_at"`
	// readOnly: true
	UpdatedAt null.NullTime `db:"updated_at"`
	// readOnly: true
	DeletedAt null.NullTime `db:"deleted_at"`
}
