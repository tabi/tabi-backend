package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
)

type Attribute struct {
	DeviceID  uint          `db:"device_id"`
	Key       string        `db:"key"`
	Value     string        `db:"value"`
	CreatedAt null.NullTime `db:"created_at"`
	UpdatedAt null.NullTime `db:"updated_at"`
	DeletedAt null.NullTime `db:"deleted_at"`
}
