package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
	"time"
)

type Track struct {
	ID        uint
	StartTime null.NullTime `db:"start_time"`
	EndTime   null.NullTime `db:"end_time"`
	DeviceId  uint          `db:"device_id"`
	LocalId   uint          `db:"local_id"`
	CreatedAt null.NullTime `db:"created_at"`
	UpdatedAt null.NullTime `db:"updated_at"`
	DeletedAt null.NullTime `db:"deleted_at"`
}

type TrackMotive struct {
	Id           uint
	DeviceId     uint           `db:"device_id"`
	TrackId      null.NullInt64 `db:"stop_id"`
	LocalTrackId null.NullInt64 `db:"local_stop_id"`
	Motive       string         `db:"motive"`
	Timestamp    time.Time      `db:"timestamp"`
	CreatedAt    null.NullTime  `db:"created_at"`
	UpdatedAt    null.NullTime  `db:"updated_at"`
	DeletedAt    null.NullTime  `db:"deleted_at"`
}
