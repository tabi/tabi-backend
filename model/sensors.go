package model

import (
	"time"
)

type Accelerometer struct {
	*MotionSensor
}

type Gyroscope struct {
	*MotionSensor
}

type Magnetometer struct {
	*MotionSensor
}

type LinearAcceleration struct {
	*MotionSensor
}

type Orientation struct {
	*MotionSensor
}

type Gravity struct {
	*MotionSensor
}

type Quaternion struct {
	*MotionSensor
	W float64
}

type MotionSensor struct {
	ID        uint64
	DeviceId  uint `db:"device_id"`
	X         float64
	Y         float64
	Z         float64
	Timestamp time.Time
}
