package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
	"golang.org/x/crypto/bcrypt"
)

// Role constants represent the possible roles an user can be assigned.
type Role int

const (
	// Regular represents a regular user
	Regular Role = iota
	// Admin represents an administrator
	Admin
)

// User represents a user that can use the api.
type User struct {
	ID         uint
	Username   string
	FirstName  null.NullString `db:"first_name"`
	LastName   null.NullString `db:"last_name"`
	Password   string
	Email      null.NullString
	Role       Role
	CreatedAt  null.NullTime `db:"created_at"`
	ModifiedAt null.NullTime `db:"modified_at"`
	DeletedAt  null.NullTime `db:"deleted_at"`
	Disabled   null.NullBool
}

// SetPassword hashes the given unhashed password, hashes it and stores it in the user.
func (u *User) SetPassword(password string) (err error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return
	}
	u.Password = string(bytes)

	return
}

// VerifyPassword accepts an unhashed password and verifies it. Returning true if the password is valid.
func (u User) VerifyPassword(password string) (valid bool) {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	return err == nil
}
