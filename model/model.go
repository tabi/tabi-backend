package model

import (
	"time"
)

// swagger:model
type PositionEntry struct {
	// readOnly: true
	ID uint
	// readOnly: true
	DeviceID                        uint
	Latitude                        float64
	Longitude                       float64
	Accuracy                        float32
	Speed                           float32
	Altitude                        float32
	Heading                         float32
	DesiredAccuracy                 float32
	DistanceBetweenPreviousPosition float32
	// swagger:strfmt date-time
	Timestamp time.Time
	// readOnly: true
	// swagger:strfmt date-time
	CreatedAt time.Time
}
