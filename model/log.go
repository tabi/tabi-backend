package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
)

type Log struct {
	ID        uint            `db:"id"`
	Origin    null.NullString `db:"origin"`
	Event     null.NullString `db:"event"`
	Message   null.NullString `db:"message"`
	UserId    uint            `db:"user_id"`
	DeviceId  uint            `db:"device_id"`
	Timestamp null.NullTime   `db:"timestamp"`
	CreatedAt null.NullTime   `db:"created_at"`
	UpdatedAt null.NullTime   `db:"updated_at"`
	DeletedAt null.NullTime   `db:"deleted_at"`
}
