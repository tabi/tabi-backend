package auth

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"gitlab.com/tabi/tabi-backend/api/messages"
	"gitlab.com/tabi/tabi-backend/model"
	"net/http"
	"strconv"
	"time"
)

type Claims struct {
	Username string `json:"username"`
	Roles    []model.Role
	jwt.StandardClaims
}

// NewToken returns a TokenResult with the signed JWT (token) with claims based on the user parameter.
func NewToken(user *model.User, signKey []byte) (result messages.TokenResult, err error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := Claims{}
	claims.ExpiresAt = time.Now().Add(time.Hour * time.Duration(1)).Unix()
	claims.NotBefore = time.Now().Unix()
	idStr := strconv.FormatUint(uint64(user.ID), 10)
	claims.Subject = idStr
	claims.Username = user.Username
	claims.Roles = []model.Role{user.Role}
	token.Claims = claims

	tokenString, err := token.SignedString(signKey)
	if err != nil {
		return
	}

	result = messages.TokenResult{Token: tokenString, UserId: user.ID}
	return
}

func TokenFromRequest(signKey []byte, r *http.Request) (claims *Claims, err error) {
	token, err := request.ParseFromRequestWithClaims(r, request.AuthorizationHeaderExtractor, &Claims{},
		func(token *jwt.Token) (interface{}, error) {
			return signKey, nil
		})
	if err != nil {
		return
	}

	claims, ok := token.Claims.(*Claims)
	if !ok || !token.Valid {
		err = errors.New("invalid token")
	}
	return
}
