package util

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"net/http"
	"time"
)

func NewEventLog(r *http.Request, event model.Event, message string, result string) model.EventLog {
	id, _ := GetClientInfoFromRequest(r)

	return model.EventLog{
		Path:      null.ToNullString(r.URL.Path),
		IpAddress: null.ToNullString(GetIPAddress(r)),
		ClientId:  null.ToNullString(id),
		Event:     null.ToNullString(string(event)),
		Message:   null.ToNullString(message),
		Result:    null.ToNullString(result),
		Timestamp: null.ToNullTime(time.Now()),
	}
}
