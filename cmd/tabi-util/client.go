package main

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/store"
)

func addClient(store store.Store, id string, key string) error {
	_, err := store.GetClient(id)

	if err == nil {
		return errors.New("Client already exists with id " + id)
	}

	if key == "" {
		key = generateKey()
		fmt.Printf("Generated a key for client:\n%s\n", key)
	}

	err = store.CreateClient(&model.Client{
		Id:      id,
		Key:     key,
		Allowed: true,
	})

	return err
}

func generateKey() string {
	genKey := make([]byte, 64)
	_, err := rand.Read(genKey)
	if err != nil {
		// handle error here
	}
	return hex.EncodeToString(genKey)
}

func removeClient(store store.Store, id string) {

}
