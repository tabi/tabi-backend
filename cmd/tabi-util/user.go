package main

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gitlab.com/tabi/tabi-backend/server"
	"gitlab.com/tabi/tabi-backend/store"
)

func CreateUserCommand(c *cli.Context) error {
	_, store := server.SetupConfigAndStore(c.GlobalString("config"))

	username := c.String("username")
	password := c.String("password")

	if username == "" || password == "" {
		fmt.Println("No username or password provided")
		return errors.New("No username or password provided")
	}

	var selectedRole model.Role
	if c.Bool("admin") {
		selectedRole = model.Admin
	} else {
		selectedRole = model.Regular
	}

	u := model.User{
		Username:  c.String("username"),
		FirstName: null.ToNullString(c.String("first-name")),
		LastName:  null.ToNullString(c.String("last-name")),
		Role:      selectedRole,
	}

	u.SetPassword(c.String("password"))

	err := store.CreateUser(&u)
	if err != nil {
		fmt.Println("Could not save user to store")
	} else {
		fmt.Println("User was created")
	}

	return nil
}

func createUser(store store.Store, username string, password string, admin bool) {

}
