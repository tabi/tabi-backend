package main

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx/types"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"os"
	"strings"
)

func SetupUserDeviceConfigCsvCommand(c *cli.Context) error {
	_, store := server.SetupConfigAndStore(c.GlobalString("config"))

	csvFile := c.String("csvfile")

	if csvFile == "" {
		fmt.Println("No csv provided")
		return errors.New("No csv provided")
	}

	f, err := os.Open(csvFile)
	defer f.Close()

	r := csv.NewReader(f)

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(records)

	var userconfigs []model.UserDeviceConfig

	csvLayout := make(map[int][]string)
	csvUsernameIndex := -1

	headers := true
	for _, v := range records {
		// check headers to see how things are layed out
		if headers {
			for i, col := range v {
				values := strings.Split(col, ".")
				csvLayout[i] = values

				if strings.Contains(col, "Username") {
					csvUsernameIndex = i
				}

			}
			headers = false
		} else {
			uConfig := model.UserDeviceConfig{}
			rootConfig := make(map[string]map[string]interface{})

			for i, col := range v {
				currentConfigItem := csvLayout[i]
				if len(currentConfigItem) > 1 {
					subConfigStr := currentConfigItem[0]
					parameterConfigStr := currentConfigItem[1]

					if _, ok := rootConfig[subConfigStr]; !ok {
						rootConfig[subConfigStr] = make(map[string]interface{})
					}

					rootConfig[subConfigStr][parameterConfigStr] = col
				}
				if i == csvUsernameIndex {
					user, err := store.GetUserByUsername(col)
					if err != nil {
						return err
					}
					uConfig.UserId = user.ID
				}

				b, err := json.Marshal(rootConfig)
				if err != nil {
					log.Fatal(err)
				}
				uConfig.Config = types.JSONText(string(b))
			}
			userconfigs = append(userconfigs, uConfig)
		}

	}

	for _, v := range userconfigs {
		err = store.CreateUserDeviceConfig(&v)
		if err != nil {
			fmt.Println("Could not save config to store")
		} else {
			fmt.Println("Config was created")
		}
	}

	return nil
}
