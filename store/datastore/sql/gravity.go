package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateGravity(gravity *model.Gravity) error {
	var insertQuery = `INSERT INTO gravities ("x", "y", "z", "timestamp", "device_id")
	VALUES ($1, $2, $3, $4, $5)`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("gravity create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("gravity create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("gravity create commit")
			return
		}
	}()

	row := tx.QueryRow(insertQuery, gravity.X, gravity.Y, gravity.Z, gravity.Timestamp, gravity.DeviceId)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("gravity create queryrow")
		return err
	}

	gravity.ID = uint64(id)

	return err
}

func (ds *datastore) CreateGravities(gravities []*model.Gravity) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("gravity create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("gravity create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("gravity create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("gravities", "device_id", "x", "y", "z", "timestamp"))

	for _, v := range gravities {
		_, err = stmt.Exec(v.DeviceId, v.X, v.Y, v.Z, v.Timestamp)

		if err != nil {
			log.WithField("error", err).Error("gravity create exec")
			return err
		}
	}

	err = stmt.Close()

	if err != nil {
		log.WithField("error", err).Error("gravity could not close statement")
	}

	return err
}

func (ds *datastore) GetGravities(deviceId uint, startTime time.Time, endTime time.Time) (gravities []*model.Gravity, err error) {
	err = ds.DB.Select(&gravities, "SELECT * FROM gravities WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("gravities get")
		return nil, err
	}

	return gravities, err
}
