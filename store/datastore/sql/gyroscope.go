package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateGyroscope(gyroscope *model.Gyroscope) error {
	var insertQuery = `INSERT INTO gyroscopes ("x", "y", "z", "timestamp", "device_id")
	VALUES ($1, $2, $3, $4, $5)`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("gyroscope create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("gyroscope create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("gyroscope create commit")
			return
		}
	}()

	row := tx.QueryRow(insertQuery, gyroscope.X, gyroscope.Y, gyroscope.Z, gyroscope.Timestamp, gyroscope.DeviceId)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("gyroscope create queryrow")
		return err
	}

	gyroscope.ID = uint64(id)

	return err
}

func (ds *datastore) CreateGyroscopes(gyroscopes []*model.Gyroscope) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("gyroscope create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("gyroscope create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("gyroscope create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("gyroscopes", "device_id", "x", "y", "z", "timestamp"))

	for _, v := range gyroscopes {
		_, err = stmt.Exec(v.DeviceId, v.X, v.Y, v.Z, v.Timestamp)

		if err != nil {
			log.WithField("error", err).Error("gyroscope create exec")
			return err
		}
	}

	err = stmt.Close()

	if err != nil {
		log.WithField("error", err).Error("gyroscope could not close statement")
	}

	return err
}

func (ds *datastore) GetGyroscopes(deviceId uint, startTime time.Time, endTime time.Time) (gyroscopes []*model.Gyroscope, err error) {
	err = ds.DB.Select(&gyroscopes, "SELECT * FROM gyroscopes WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("gyroscopes get")
		return nil, err
	}

	return gyroscopes, err
}
