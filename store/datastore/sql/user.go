package sql

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

var createUserQuery = `INSERT INTO users (username, first_name, last_name, password, email, role, created_at)
VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id`

func (ds *datastore) CreateUser(user *model.User) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("user create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("user create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("user create commit")
			return
		}
	}()

	row := tx.QueryRow(createUserQuery, user.Username, user.FirstName, user.LastName, user.Password, user.Email, user.Role, time.Now())

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("user create queryrow")
		return err
	}

	user.ID = uint(id)

	return err
}

var updateUserQuery = `UPDATE users SET (username, first_name, last_name, password, email, role, modified_at) =
 ($2, $3, $4, $5, $6, $7, $8) WHERE id = $1`

func (ds *datastore) UpdateUser(user *model.User) error {
	if user.ID == 0 {
		return errors.New("empty id/pk")
	}

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("user update tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("user update rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("user update commit")
			return
		}
	}()

	res, err := tx.Exec(updateUserQuery, user.ID, user.Username, user.FirstName, user.LastName, user.Password, user.Email, user.Role, time.Now())

	if err != nil {
		log.WithField("error", err).Error("user update exec")
		return err
	}
	rows, err := res.RowsAffected()

	if err != nil {
		log.WithField("error", err).Error("user update rows affected")
		return err
	}

	if rows != int64(1) {
		log.Error("user create update more rows")
		return errors.New("more rows update than should be")
	}

	return err
}

func (ds *datastore) GetUser(id uint) (*model.User, error) {
	if id == 0 {
		return nil, errors.New("empty id/pk")
	}

	var user model.User

	err := ds.DB.Get(&user, "SELECT * FROM users WHERE id = $1", id)

	if err != nil {
		log.WithField("error", err).Error("user get")
		return nil, err
	}

	return &user, nil
}

func (ds *datastore) GetUserList() ([]*model.User, error) {
	var users []*model.User

	err := ds.DB.Select(&users, "SELECT * FROM users")

	if err != nil {
		log.WithField("error", err).Error("user list get")
		return nil, err
	}

	return users, nil
}

func (ds *datastore) GetUserByUsername(username string) (*model.User, error) {
	var user model.User

	err := ds.DB.Get(&user, "SELECT * FROM users WHERE username = $1", username)

	if err != nil {
		log.WithField("error", err).Error("user get")
		return nil, err
	}

	return &user, nil
}
