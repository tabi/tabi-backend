package sql

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
)

var createEventLog = `INSERT INTO event_logs (user_id, event, message, path, result, ip_address, client_id, timestamp)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`

func (ds *datastore) CreateEvent(event *model.EventLog) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("event create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("event create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("event create commit")
			return
		}
	}()

	tx.MustExec(createEventLog, event.UserId, event.Event, event.Message, event.Path, event.Result, event.IpAddress, event.ClientId, event.Timestamp)

	return err
}

func (ds *datastore) GetTokenGeneratedUniqueEvents() ([]*model.EventLog, error) {
	var getQuery = `SELECT DISTINCT ON (event_logs.user_id) event_logs.user_id, users.username, event_logs.* 
FROM event_logs
INNER JOIN users ON users.id=event_logs.user_id
WHERE event_logs.event = 'TokenGenerated'
ORDER BY event_logs.user_id;`

	var events []*model.EventLog

	err := ds.Select(&events, getQuery)

	if err != nil {
		log.WithField("error", err).Error("devices get")
		return nil, err
	}

	return events, nil
}
