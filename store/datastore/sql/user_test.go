package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreateUser(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	user := model.User{FirstName: null.ToNullString("First"), LastName: null.ToNullString("Last"), Email: null.ToNullString("some@email.ext"), Role: 0, Password: "SomePassword", Username: "username"}
	var lastInsertId uint = 32

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO users").
		WithArgs(user.Username, user.FirstName, user.LastName, user.Password, user.Email, user.Role, AnyTime{}).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(lastInsertId))
	mock.ExpectCommit()

	err = ds.CreateUser(&user)

	if user.ID != lastInsertId {
		t.Errorf("pk not returned in user properly")
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldUpdateUser(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	user := model.User{ID: 1, FirstName: null.ToNullString("First"), LastName: null.ToNullString("Last"), Email: null.ToNullString("some@email.ext"), Role: 0, Password: "SomePassword", Username: "username"}

	mock.ExpectBegin()
	mock.ExpectExec("UPDATE users").
		WithArgs(user.ID, user.Username, user.FirstName, user.LastName, user.Password, user.Email, user.Role, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(int64(user.ID), 1))
	mock.ExpectCommit()

	err = ds.UpdateUser(&user)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestFailUpdateUserOnEmptyId(t *testing.T) {
	db, _, _ := createMockSqlxDb()
	ds := createDatastore(db)

	expectedError := "empty id/pk"
	user := model.User{FirstName: null.ToNullString("First"), LastName: null.ToNullString("Last"), Email: null.ToNullString("some@email.ext"), Role: 0, Password: "SomePassword", Username: "username"}

	actualErr := ds.UpdateUser(&user)

	if actualErr == nil {
		t.Errorf("%s was expected. No error occurred", expectedError)
	}

	if actualErr.Error() != expectedError {
		t.Errorf("unexpected error. expected: %s actual %s", expectedError, actualErr)
	}
}

func TestShouldGetUser(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	expectedUser := model.User{ID: 21, FirstName: null.ToNullString("First"), LastName: null.ToNullString("last"), Email: null.ToNullString("some@email.ext"), Role: 0, Password: "SomePassword", Username: "username"}

	userId := 21

	columns := []string{"id", "username", "first_name", "last_name", "password", "email", "role", "disabled", "created_at", "modified_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM users").
		WithArgs(userId).
		WillReturnRows(sqlmock.NewRows(columns).AddRow("21", "username", "First", "last", "SomePassword", "some@email.ext", 0, false, time.Now(), time.Now(), time.Time{}))

	actualUser, err := ds.GetUser(21)

	if actualUser.FirstName != expectedUser.FirstName {
		t.Errorf("expected %s. actual %s.", expectedUser.FirstName.String, actualUser.FirstName.String)
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetUserList(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "username", "first_name", "last_name", "password", "email", "role", "disabled", "created_at", "modified_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM users").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow("21", "username", "First", "last", "SomePassword", "some@email.ext", 0, false, time.Now(), time.Now(), time.Time{}).
			AddRow("22", "username2", "First2", "last2", "SomePassword2", "some2@email.ext", 0, false, time.Now(), time.Now(), time.Time{}))

	actualUsers, err := ds.GetUserList()

	if len(actualUsers) != 2 {
		t.Errorf("expected %d. actual %d.", 2, len(actualUsers))
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
