package sql

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"time"
)

func (ds *datastore) CreateAttribute(attribute *model.Attribute) error {
	var insertQuery = `INSERT INTO attributes (device_id, key, value, created_at, updated_at)
						VALUES ($1, $2, $3, $4, $5)`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("attribute create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("attribute create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("attribute create commit")
			return
		}
	}()

	createdTime := time.Now()

	_, err = tx.Exec(insertQuery, attribute.DeviceID, attribute.Key, attribute.Value, createdTime, createdTime)

	if err != nil {
		log.WithField("error", err).Error("attribute create exec")
		return err
	}

	attribute.CreatedAt = null.ToNullTime(createdTime)

	return err
}

func (ds *datastore) GetAttribute(deviceId uint, attributeKey string) (*model.Attribute, error) {
	var getQuery = `SELECT *
					FROM attributes
					WHERE device_id = $1
					AND key = $2
					ORDER BY created_at DESC
					LIMIT 1`

	if deviceId == 0 {
		return nil, errors.New("empty device id")
	}
	if attributeKey == "" {
		return nil, errors.New("empty attribute key")
	}

	var attribute model.Attribute

	err := ds.Get(&attribute, getQuery, deviceId, attributeKey)

	if err != nil {
		log.WithField("error", err).Error("attribute get")
		return nil, err
	}

	return &attribute, nil
}

func (ds *datastore) GetAttributeList(deviceId uint) ([]*model.Attribute, error) {

	var getAllQuery = `SELECT * 
						FROM attributes
						WHERE device_id = $1`

	var attributes []*model.Attribute

	err := ds.Select(&attributes, getAllQuery, deviceId)

	if err != nil {
		log.WithField("error", err).Error("attributes get")
		return nil, err
	}

	return attributes, nil
}
