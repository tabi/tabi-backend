package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateOrientation(orientation *model.Orientation) error {
	var insertQuery = `INSERT INTO orientations ("x", "y", "z", "timestamp", "device_id")
	VALUES ($1, $2, $3, $4, $5)`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("orientation create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("orientation create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("orientation create commit")
			return
		}
	}()

	row := tx.QueryRow(insertQuery, orientation.X, orientation.Y, orientation.Z, orientation.Timestamp, orientation.DeviceId)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("orientation create queryrow")
		return err
	}

	orientation.ID = uint64(id)

	return err
}

func (ds *datastore) CreateOrientations(orientations []*model.Orientation) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("orientation create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("orientation create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("orientation create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("orientations", "device_id", "x", "y", "z", "timestamp"))

	for _, v := range orientations {
		_, err = stmt.Exec(v.DeviceId, v.X, v.Y, v.Z, v.Timestamp)

		if err != nil {
			log.WithField("error", err).Error("orientation create exec")
			return err
		}
	}

	err = stmt.Close()

	if err != nil {
		log.WithField("error", err).Error("orientation could not close statement")
	}

	return err
}

func (ds *datastore) GetOrientations(deviceId uint, startTime time.Time, endTime time.Time) (orientations []*model.Orientation, err error) {
	err = ds.DB.Select(&orientations, "SELECT * FROM orientations WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("orientations get")
		return nil, err
	}

	return orientations, err
}
