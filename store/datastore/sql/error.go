package sql

import "fmt"

type noDeviceId struct {
	arg  int
	prob string
}

func (e *noDeviceId) Error() string {
	return fmt.Sprintf("%d - %s", e.arg, e.prob)
}
