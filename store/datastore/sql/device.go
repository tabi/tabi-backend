package sql

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"time"
)

func (ds *datastore) CreateDevice(device *model.Device) error {
	var insertQuery = `INSERT INTO devices (user_id, manufacturer, model, os, os_version, created_at)
	VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("device create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("device create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("device create commit")
			return
		}
	}()

	createdTime := time.Now()

	row := tx.QueryRow(insertQuery, device.UserId, device.Manufacturer, device.Model, device.OperatingSystem, device.OperatingSystemVersion, createdTime)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("device create exec")
		return err
	}

	device.ID = uint(id)
	device.CreatedAt = null.ToNullTime(createdTime)

	return err
}

func (ds *datastore) UpdateDevice(device *model.Device) error {
	var updateQuery = `UPDATE devices 
						SET(user_id = $1, manufacturer = $2, model = $2, os = $3, os_version = $4, updated_at = $5, deleted_at = $6)
						WHERE id = $7`

	if device.ID == 0 {
		return errors.New("empty id/pk")
	}

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("device update tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("device update rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("device update commit")
			return
		}
	}()

	res, err := tx.Exec(updateQuery, time.Now(), device.DeletedAt, device.UserId, device.Manufacturer, device.Model, device.OperatingSystem, device.ID)
	if err != nil {
		log.WithField("error", err).Error("device update exec")
		return err
	}

	rows, err := res.RowsAffected()
	if err != nil {
		log.WithField("error", err).Error("device update rows affected")
		return err
	}
	if rows != int64(1) {
		log.Error("device update affected more rows")
		return errors.New("more rows affected than should be")
	}

	return err
}

func (ds *datastore) GetDevice(id uint) (*model.Device, error) {
	var getQuery = `SELECT *
					FROM devices
					WHERE id = $1`

	if id == 0 {
		return nil, errors.New("empty id/pk")
	}

	var device model.Device

	err := ds.Get(&device, getQuery, id)

	if err != nil {
		log.WithField("error", err).Error("device get")
		return nil, err
	}

	return &device, nil
}

func (ds *datastore) GetDevices() ([]*model.Device, error) {

	var getAllQuery = `SELECT * FROM devices`

	var devices []*model.Device

	err := ds.Select(&devices, getAllQuery)

	if err != nil {
		log.WithField("error", err).Error("devices get")
		return nil, err
	}

	return devices, nil
}

func (ds *datastore) GetDevicesByUserId(userId uint) ([]*model.Device, error) {
	var getQuery = `SELECT *
					FROM devices
					WHERE user_id = $1`

	if userId == 0 {
		return nil, errors.New("empty user id")
	}

	var devices []*model.Device

	err := ds.Select(&devices, getQuery, userId)

	if err != nil {
		log.WithField("error", err).Error("devices get")
		return nil, err
	}

	return devices, nil
}
