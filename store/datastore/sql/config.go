package sql

import (
	"encoding/json"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model/null"
	"time"

	"gitlab.com/tabi/tabi-backend/model"
)

func (ds *datastore) GetConfigByUserId(userId uint) (*model.UserDeviceConfig, error) {
	var getQuery = `SELECT *
					FROM user_device_config
					WHERE user_id = $1`

	if userId == 0 {
		return nil, errors.New("empty id/pk")
	}

	var device model.UserDeviceConfig

	err := ds.Get(&device, getQuery, userId)

	if err != nil {
		log.WithField("error", err).Error("user device config get")
		return nil, err
	}

	return &device, nil
}

func (ds *datastore) CreateUserDeviceConfig(userDeviceConfig *model.UserDeviceConfig) error {
	var insertQuery = `INSERT INTO user_device_config (user_id, config, created_at)
	VALUES ($1, $2, $3) RETURNING id`

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("user device config create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("user device config create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("user device config create commit")
			return
		}
	}()

	createdTime := time.Now()

	jsonConfig, err := json.Marshal(userDeviceConfig.Config)
	if err != nil {
		return errors.New("json invalid")
	}

	row := tx.QueryRow(insertQuery, userDeviceConfig.UserId, string(jsonConfig), createdTime)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("user device config create exec")
		return err
	}

	userDeviceConfig.ID = uint(id)
	userDeviceConfig.CreatedAt = null.ToNullTime(createdTime)

	return err
}
