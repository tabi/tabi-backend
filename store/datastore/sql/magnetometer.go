package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateMagnetometer(magnetometer *model.Magnetometer) error {
	var insertQuery = `INSERT INTO magnetometers ("x", "y", "z", "timestamp", "device_id")
	VALUES ($1, $2, $3, $4, $5)`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("magnetometer create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("magnetometer create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("magnetometer create commit")
			return
		}
	}()

	row := tx.QueryRow(insertQuery, magnetometer.X, magnetometer.Y, magnetometer.Z, magnetometer.Timestamp, magnetometer.DeviceId)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("magnetometer create queryrow")
		return err
	}

	magnetometer.ID = uint64(id)

	return err
}

func (ds *datastore) CreateMagnetometers(magnetometers []*model.Magnetometer) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("magnetometer create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("magnetometer create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("magnetometer create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("magnetometers", "device_id", "x", "y", "z", "timestamp"))

	for _, v := range magnetometers {
		_, err = stmt.Exec(v.DeviceId, v.X, v.Y, v.Z, v.Timestamp)

		if err != nil {
			log.WithField("error", err).Error("magnetometer create exec")
			return err
		}
	}

	err = stmt.Close()

	if err != nil {
		log.WithField("error", err).Error("magnetometer could not close statement")
	}

	return err
}

func (ds *datastore) GetMagnetometers(deviceId uint, startTime time.Time, endTime time.Time) (magnetometers []*model.Magnetometer, err error) {
	err = ds.DB.Select(&magnetometers, "SELECT * FROM magnetometers WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("magnetometers get")
		return nil, err
	}

	return magnetometers, err
}
