package sql

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"

	"github.com/lib/pq"
)

func (ds *datastore) CreateBatteryInfo(battery *model.BatteryInfo) error {
	err := ds.CreateBatteryInfos([]*model.BatteryInfo{battery})

	return err
}

func (ds *datastore) CreateBatteryInfos(batteries []*model.BatteryInfo) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("battery create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("battery create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("battery create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("battery_infos", "device_id", "battery_level", "state", "timestamp", "created_at"))

	for _, v := range batteries {
		_, err = stmt.Exec(v.DeviceId, v.BatteryLevel, v.State, v.Timestamp, time.Now())

		if err != nil {
			log.WithField("error", err).Error("battery create exec")
			return err
		}
	}

	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("battery create stmt close")
	}

	return err
}

func (ds *datastore) GetBatteryInfos(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.BatteryInfo, error) {
	var batteries []*model.BatteryInfo
	err := ds.DB.Select(&batteries, "SELECT * FROM battery_infos WHERE device_id = $1 AND (timestamp BETWEEN $2 AND $3)", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("battery get")
		return nil, err
	}

	return batteries, err
}
