package migrations

type migrationMap struct {
	migrations map[string]string
	keys       []string
}

var migrations migrationMap

func init() {
	migrations.migrations = make(map[string]string)
	migrations.addMigration("initial-create", m0001initialCreate)
	migrations.addMigration("add-motives", m0002addMotives)
	migrations.addMigration("stop-motives-references-stop-visits", m0003stopMotivesReferencesStopVisits)
	migrations.addMigration("transportmode-variable", m0004transportModeVariable)
	migrations.addMigration("alterAttributesPrimaryKey", m0005alterAttributesPrimaryKey)
	migrations.addMigration("M0006-Add-Event-Logs", m0006addEventLogsIndexes)
	migrations.addMigration("M0007-Add-Clients", m0007addClients)
	migrations.addMigration("M0008-Add-Config", m0008addConfig)
	migrations.addMigration("M0009-Add-Questions-Answers", m0009AddQuestions)
}

func (m *migrationMap) addMigration(key string, value string) {
	m.migrations[key] = value
	m.keys = append(m.keys, key)
}

func (m *migrationMap) IterateOverMigrations(migrateFunc func(key string, value string)) {
	for _, key := range m.keys {
		migrateFunc(key, m.migrations[key])
	}
}

func (m *migrationMap) Lookup(id string) string {
	return m.migrations[id]
}

func ReturnMigrations() migrationMap {
	return migrations
}
