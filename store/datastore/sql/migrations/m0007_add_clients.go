package migrations

var m0007addClients = `
CREATE TABLE clients (
id text NOT NULL PRIMARY KEY,
key text NOT NULL,
allowed bool NOT NULL,
"created_at" timestamp with time zone,
"updated_at" timestamp with time zone,
"deleted_at" timestamp with time zone
);
`
