package migrations

var m0002addMotives = `
ALTER TABLE user_stops ADD COLUMN "timestamp" timestamp with time zone;

CREATE TABLE user_stop_motives (
id serial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
stop_visit_id integer REFERENCES user_stops(id) ON DELETE CASCADE,
local_stop_visit_id integer,
motive text,
"timestamp" timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);

CREATE TABLE track_motives (
id serial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
track_id integer REFERENCES tracks(id) ON DELETE CASCADE,
local_track_id integer,
motive text,
"timestamp" timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);
`
