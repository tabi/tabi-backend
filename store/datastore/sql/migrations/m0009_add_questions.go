package migrations

var m0009AddQuestions = `
CREATE TABLE questions_answers (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
identifier text,
answer text,
question_date timestamp with time zone,
timestamp timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);
`
