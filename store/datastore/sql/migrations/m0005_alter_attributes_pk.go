package migrations

var m0005alterAttributesPrimaryKey = `
ALTER TABLE attributes DROP CONSTRAINT attributes_pkey;
ALTER TABLE attributes ADD PRIMARY KEY (device_id, key, created_at);
`
