package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreateTracks(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	track := model.Track{DeviceId: 1,
		LocalId:   3,
		StartTime: null.ToNullTime(time.Now()),
		EndTime:   null.ToNullTime(time.Now()),
		CreatedAt: null.ToNullTime(time.Now())}

	tracks := []*model.Track{&track}

	mock.ExpectBegin()
	mock.ExpectPrepare("COPY").
		ExpectExec().
		WithArgs(track.DeviceId, track.LocalId, AnyTime{}, AnyTime{}, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = ds.CreateTracks(tracks)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetTracks(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "device_id", "local_id", "start_time", "end_time", "created_at", "updated_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM tracks").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow(1, 1, 3, time.Now(), time.Now(), time.Now(), time.Time{}, time.Time{}).
			AddRow(2, 1, 4, time.Now(), time.Now(), time.Now(), time.Time{}, time.Time{}))

	actualTracks, err := ds.GetTracks(1, time.Time{}, time.Now())

	if length := len(actualTracks); length != 2 {
		t.Errorf("expected %d. actual %d.", 2, length)
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
