package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateAccelerometer(accelerometer *model.Accelerometer) error {
	var insertQuery = `INSERT INTO accelerometers ("x", "y", "z", "timestamp", "device_id")
	VALUES ($1, $2, $3, $4, $5)`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("accelerometer create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("accelerometer create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("accelerometer create commit")
			return
		}
	}()

	row := tx.QueryRow(insertQuery, accelerometer.X, accelerometer.Y, accelerometer.Z, accelerometer.Timestamp, accelerometer.DeviceId)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("accelerometer create queryrow")
		return err
	}

	accelerometer.ID = uint64(id)

	return err
}

func (ds *datastore) CreateAccelerometers(accelerometers []*model.Accelerometer) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("accelerometer create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("accelerometer create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("accelerometer create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("accelerometers", "device_id", "x", "y", "z", "timestamp"))

	for _, v := range accelerometers {
		_, err = stmt.Exec(v.DeviceId, v.X, v.Y, v.Z, v.Timestamp)

		if err != nil {
			log.WithField("error", err).Error("accelerometer create exec")
			return err
		}
	}

	err = stmt.Close()

	if err != nil {
		log.WithField("error", err).Error("accelerometer could not close statement")
	}

	return err
}

func (ds *datastore) GetAccelerometers(deviceId uint, startTime time.Time, endTime time.Time) (accelerometers []*model.Accelerometer, err error) {
	err = ds.DB.Select(&accelerometers, "SELECT * FROM accelerometers WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("accelerometers get")
		return nil, err
	}

	return accelerometers, err
}
