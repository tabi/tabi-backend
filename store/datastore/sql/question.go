package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateQuestions(questions []*model.Question) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("question answers create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("question answers create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("question answers create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(
		pq.CopyIn("questions_answers",
			"device_id",
			"identifier",
			"answer",
			"question_date",
			"timestamp",
			"created_at"))

	for _, v := range questions {
		_, err = stmt.Exec(v.DeviceId, v.Identifier, v.Answer,
			v.QuestionDate, v.Timestamp, time.Now())

		if err != nil {
			log.WithField("error", err).Error("question answers create exec")
			return err
		}
	}
	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("question answers create stmt close")
		return err
	}

	return err
}
