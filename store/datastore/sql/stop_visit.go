package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"

	"errors"
	"time"
)

func (ds *datastore) CreateStopVisit(visit *model.UserStopVisit) error {
	err := ds.CreateStopVisits([]*model.UserStopVisit{visit})

	return err
}

func (ds *datastore) CreateStopVisits(visits []*model.UserStopVisit) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("stopvisits create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("stopvisits create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("stopvisits create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("user_stop_visits",
		"device_id",
		"local_id",
		"stop_id",
		"local_stop_id",
		"begin_timestamp",
		"end_timestamp",
		"created_at"))

	for _, v := range visits {
		_, err = stmt.Exec(v.DeviceId, v.LocalId, v.StopId, v.LocalStopId, v.BeginTimestamp, v.EndTimestamp, time.Now())

		if err != nil {
			log.WithField("error", err).Error("stopvisits create exec")
			return err
		}
	}

	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("stopvisits create stmt close")
	}

	return err
}

func (ds *datastore) GetStopVisit(id uint) (*model.UserStopVisit, error) {
	var getQuery = `SELECT *
					FROM user_stop_visits
					WHERE id = $1`

	if id == 0 {
		return nil, errors.New("empty id/pk")
	}

	var visit model.UserStopVisit

	err := ds.Get(&visit, getQuery, id)

	if err != nil {
		log.WithField("error", err).Error("visit get")
		return nil, err
	}

	return &visit, nil
}

func (ds *datastore) GetStopVisitByDeviceAndLocalId(deviceId uint, localId uint) (*model.UserStopVisit, error) {
	var getQuery = `SELECT *
					FROM user_stop_visits
					WHERE device_id = $1
					AND local_id = $2`

	if deviceId == 0 || localId == 0 {
		return nil, errors.New("empty id/pk")
	}

	var visit model.UserStopVisit

	err := ds.Get(&visit, getQuery, deviceId, localId)

	if err != nil {
		log.WithField("error", err).Error("visit get")
		return nil, err
	}

	return &visit, nil
}

func (ds *datastore) GetStopVisitsByDevice(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.UserStopVisit, error) {
	var visits []*model.UserStopVisit

	var selectQuery = `SELECT * FROM user_stop_visits
					WHERE device_id = $1 AND (begin_timestamp >= $2 AND begin_timestamp =< $2) 
					OR (end_timestamp >= $2 AND end_timestamp =< $2)`

	err := ds.DB.Select(&visits,
		selectQuery, deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("logs get")
		return nil, err
	}

	return visits, err
}
