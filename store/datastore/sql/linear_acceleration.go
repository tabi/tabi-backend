package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateLinearAcceleration(linear_acceleration *model.LinearAcceleration) error {
	var insertQuery = `INSERT INTO linear_accelerations ("x", "y", "z", "timestamp", "device_id")
	VALUES ($1, $2, $3, $4, $5)`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("linear_acceleration create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("linear_acceleration create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("linear_acceleration create commit")
			return
		}
	}()

	row := tx.QueryRow(insertQuery, linear_acceleration.X, linear_acceleration.Y, linear_acceleration.Z, linear_acceleration.Timestamp, linear_acceleration.DeviceId)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("linear_acceleration create queryrow")
		return err
	}

	linear_acceleration.ID = uint64(id)

	return err
}

func (ds *datastore) CreateLinearAccelerations(linear_accelerations []*model.LinearAcceleration) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("linear_acceleration create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("linear_acceleration create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("linear_acceleration create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("linear_accelerations", "device_id", "x", "y", "z", "timestamp"))

	for _, v := range linear_accelerations {
		_, err = stmt.Exec(v.DeviceId, v.X, v.Y, v.Z, v.Timestamp)

		if err != nil {
			log.WithField("error", err).Error("linear_acceleration create exec")
			return err
		}
	}

	err = stmt.Close()

	if err != nil {
		log.WithField("error", err).Error("linear_acceleration could not close statement")
	}

	return err
}

func (ds *datastore) GetLinearAccelerations(deviceId uint, startTime time.Time, endTime time.Time) (linear_accelerations []*model.LinearAcceleration, err error) {
	err = ds.DB.Select(&linear_accelerations, "SELECT * FROM linear_accelerations WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("linear_accelerations get")
		return nil, err
	}

	return linear_accelerations, err
}
