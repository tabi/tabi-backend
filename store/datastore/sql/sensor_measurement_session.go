package sql

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"

	"github.com/lib/pq"
)

func (ds *datastore) CreateSensorMeasurementSession(session *model.SensorMeasurementSession) error {
	err := ds.CreateSensorMeasurementSessions([]*model.SensorMeasurementSession{session})
	return err
}

func (ds *datastore) CreateSensorMeasurementSessions(sessions []*model.SensorMeasurementSession) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("sensormeasurementsession create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("sensormeasurementsession create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("sensormeasurementsession create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(
		pq.CopyIn("sensor_measurement_sessions",
			"device_id", "ambient_light", "pedometer", "proximity",
			"battery_level", "battery_status", "timestamp", "created_at"))

	for _, v := range sessions {
		_, err = stmt.Exec(v.DeviceId, v.AmbientLight, v.Pedometer, v.Proximity, v.BatteryLevel, v.BatteryStatus, v.Timestamp, time.Now())

		if err != nil {
			log.WithField("error", err).Error("sensormeasurementsession create exec")
			return err
		}
	}

	err = stmt.Close()

	if err != nil {
		log.WithField("error", err).Error("sensormeasurementsession could not close statement")
	}

	return err
}

func (ds *datastore) GetSensorMeasurementSessions(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.SensorMeasurementSession, error) {
	var sensors []*model.SensorMeasurementSession

	err := ds.DB.Select(&sensors, "SELECT * FROM sensor_measurement_sessions WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("sensormeasurementsession get")
		return nil, err
	}

	return sensors, err
}
