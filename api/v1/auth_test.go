package v1

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/golang/mock/gomock"
	"gitlab.com/tabi/tabi-backend/api/messages"
	"gitlab.com/tabi/tabi-backend/config"
	"gitlab.com/tabi/tabi-backend/mocks"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestRegisterSuccessful(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	message := &messages.UserMessage{Username: "user1", Password: "amazing"}
	resultUser := &model.User{Username: "user1"}
	resultUser.SetPassword("amazing")

	var createdUser *model.User

	mockStore := mocks.NewMockStore(mockCtrl)

	mockStore.EXPECT().GetUserByUsername(gomock.Any()).Return(nil, errors.New("error")).Times(1)

	mockStore.EXPECT().CreateUser(gomock.Any()).Return(nil).Times(1).Do(func(cUser *model.User) {
		createdUser = cUser
	})

	server := &server.Server{Store: mockStore, Conf: &config.Config{SecretKey: "verysecret"}}

	jsonMessage, err := json.Marshal(message)
	if err != nil {
		t.Fatalf(`Error is not nil ("err")`)
	}

	request, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(jsonMessage))
	response := httptest.NewRecorder()

	PostRegister(server)(response, request, nil)

	checkResponseCode(t, http.StatusOK, response.Code)
	if message.Username != createdUser.Username {
		t.Errorf("Expected username to match %v, got instead %v", message.Username, createdUser.Username)

	}
}

func TestLoginSuccessful(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	message := &messages.UserMessage{Username: "user1", Password: "amazing"}
	resultUser := &model.User{Username: "user1", ID: 20}
	resultUser.SetPassword("amazing")

	mockStore := mocks.NewMockStore(mockCtrl)
	mockStore.EXPECT().GetUserByUsername("user1").Return(resultUser, nil).Times(1)
	mockStore.EXPECT().CreateEvent(gomock.Any()).Return(nil).Times(1)

	server := &server.Server{Store: mockStore, Conf: &config.Config{SecretKey: "verysecret"}}

	jsonMessage, err := json.Marshal(message)
	if err != nil {
		t.Fatalf(`Error is not nil ("err")`)
	}

	request, _ := http.NewRequest("POST", "/token", bytes.NewBuffer(jsonMessage))
	response := httptest.NewRecorder()
	PostNewToken(server)(response, request, nil)
	checkResponseCode(t, http.StatusOK, response.Code)

	var resultMessage messages.TokenResult

	json.NewDecoder(response.Body).Decode(&resultMessage)
	if resultMessage.UserId != 20 {
		t.Errorf("Expected user id to be %v instead of %v", resultUser.ID, resultMessage.UserId)
	}
	if resultMessage.Token == "" {
		t.Errorf("Expected token to be not empty %v", resultMessage.Token)
	}
}

func TestLoginWrongLogin(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	message := &messages.UserMessage{Username: "user1", Password: "incorrectpassword"}
	resultUser := &model.User{Username: "user1", ID: 20}
	resultUser.SetPassword("amazing")

	mockStore := mocks.NewMockStore(mockCtrl)
	mockStore.EXPECT().GetUserByUsername("user1").Return(resultUser, nil).Times(1)

	server := &server.Server{Store: mockStore, Conf: &config.Config{SecretKey: "verysecret"}}

	jsonMessage, err := json.Marshal(message)
	if err != nil {
		t.Fatalf(`Error is not nil ("err")`)
	}

	request, _ := http.NewRequest("POST", "/token", bytes.NewBuffer(jsonMessage))
	response := httptest.NewRecorder()
	PostNewToken(server)(response, request, nil)
	checkResponseCode(t, http.StatusUnauthorized, response.Code)

	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Fatalf(`Error is not nil ("err")`)
	}
	bodyString := string(bodyBytes)

	bodyString = strings.TrimRight(bodyString, "\n")

	if bodyString != "Incorrect information" {
		t.Errorf("Expected incorrect information back got %v", bodyString)
	}
}
