package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/api/messages"
	_ "gitlab.com/tabi/tabi-backend/api/swagger/responses"
	"gitlab.com/tabi/tabi-backend/auth"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gitlab.com/tabi/tabi-backend/server"
	"gitlab.com/tabi/tabi-backend/util"
	"net/http"
	"time"
)

// swagger:route POST /register users registerUser
//
// Register a new user
//
// This will create a new user.
//
//     Security: []
//
//     Responses:
//       default: genericError
//       200: TokenResult

// PostRegister is the handler for the POST request that creates a new user.
func PostRegister(s *server.Server) httprouter.Handle {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		u := new(messages.UserMessage)
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}
		w.Header().Set("Content-Type", "application/json")

		err := json.NewDecoder(r.Body).Decode(u)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		user := model.User{Username: u.Username,
			Email:     null.ToNullString(u.Email),
			FirstName: null.ToNullString(u.FirstName),
			LastName:  null.ToNullString(u.LastName)}

		user.SetPassword(u.Password)

		retrievedUser, _ := s.Store.GetUserByUsername(user.Username)
		if retrievedUser != nil {
			http.Error(w, "User already exists", 400)
			return
		}

		err = s.Store.CreateUser(&user)
		if err != nil {
			http.Error(w, "An error occurred registering user", 500)
			return
		}
	}
}

// swagger:route POST /token users loginUser
//
// Get a Bearer token for a user
//
// This will show all available pets by default.
// You can get the pets that are out of stock
//
//     Security: []
//
//     Responses:
//       default: genericError
//       200: TokenMessageResponse

func PostNewToken(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var u messages.UserLoginMessage
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			sendMetric(s, "failure")
			return
		}
		err := json.NewDecoder(r.Body).Decode(&u)
		if err != nil {
			http.Error(w, "Incorrectly formatted body", 400)
			sendMetric(s, "failure")
			return
		}

		user, err := s.Store.GetUserByUsername(u.Username)
		if err != nil {
			http.Error(w, "error", 500)
			sendMetric(s, "failure")
			return
		}

		start := time.Now()

		valid := user.VerifyPassword(u.Password)

		elapsed := time.Since(start)
		log.Printf("Bcrypt took %s", elapsed)

		if !valid {
			http.Error(w, "Incorrect information", http.StatusBadRequest)
			sendMetric(s, "failure")
			return
		}

		token, _ := auth.NewToken(user, []byte(s.Conf.SecretKey))
		log.WithFields(log.Fields{"id": user.ID, "username": user.Username}).Info("Created token for user")

		e := util.NewEventLog(r, model.Event(model.TokenGenerated), "", "accepted")
		e.UserId = null.ToNullInt64(int64(user.ID))
		s.Store.CreateEvent(&e)

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(token)

		sendMetric(s, "success")
	}
}

func sendMetric(s *server.Server, status string) {
	if s != nil && s.Metrics.Logins != nil {
		s.Metrics.Logins.WithLabelValues(status).Inc()
	}
}
