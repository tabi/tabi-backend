package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
)

type PingMessage struct {
	Available bool
	Message   string
}

func GetPing(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		ping := &PingMessage{Available: true}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(ping)
	}
}
