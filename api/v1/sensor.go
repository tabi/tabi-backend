package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
)

func PostLinearAccelerationData(s *server.Server) httprouter.Handle {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, params)

		if err != nil {
			http.Error(writer, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var linearAccelerationData []*model.LinearAcceleration

		if request.Body == nil {
			http.Error(writer, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		if err := json.NewDecoder(request.Body).Decode(&linearAccelerationData); err != nil {
			http.Error(writer, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json linearAccelerations")
			return
		}

		for _, linearAcceleration := range linearAccelerationData {
			linearAcceleration.DeviceId = device.ID
		}

		err = s.Store.CreateLinearAccelerations(linearAccelerationData)

		if err != nil {
			log.WithField("error", err).Error("could not save linearAccelerationData")
			http.Error(writer, "could not save linearAccelerationData", 500)
		}
	}
}

func PostOrientationData(s *server.Server) httprouter.Handle {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, params)

		if err != nil {
			http.Error(writer, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var orientationData []*model.Orientation

		if request.Body == nil {
			http.Error(writer, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		if err := json.NewDecoder(request.Body).Decode(&orientationData); err != nil {
			http.Error(writer, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json orientationData")
			return
		}
		for _, orientation := range orientationData {
			orientation.DeviceId = device.ID
		}

		err = s.Store.CreateOrientations(orientationData)

		if err != nil {
			log.WithField("error", err).Error("could not save orientationData")
			http.Error(writer, "could not save orientationData", 500)
		}
	}
}

func PostGravityData(s *server.Server) httprouter.Handle {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

		device, err := CheckUserDeviceQueryParameters(s.Store, params)

		if err != nil {
			http.Error(writer, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		if request.Body == nil {
			http.Error(writer, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		var gravityData []*model.Gravity

		if err := json.NewDecoder(request.Body).Decode(&gravityData); err != nil {
			http.Error(writer, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json gravityData")
			return
		}

		for _, gravity := range gravityData {
			gravity.DeviceId = device.ID
		}

		err = s.Store.CreateGravities(gravityData)

		if err != nil {
			log.WithField("error", err).Error("could not save gravityData")
			http.Error(writer, "could not save gravityData", 500)
		}
	}
}

func PostQuaternionData(s *server.Server) httprouter.Handle {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

		device, err := CheckUserDeviceQueryParameters(s.Store, params)

		if err != nil {
			http.Error(writer, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		if request.Body == nil {
			http.Error(writer, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		var quaternionData []*model.Quaternion

		if err := json.NewDecoder(request.Body).Decode(&quaternionData); err != nil {
			http.Error(writer, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json quaternionData")
			return
		}

		for _, quaternion := range quaternionData {
			quaternion.DeviceId = device.ID
		}

		err = s.Store.CreateQuaternions(quaternionData)

		if err != nil {
			log.WithField("error", err).Error("could not save quaternionData")
			http.Error(writer, "could not save quaternionData", 500)
		}
	}
}

func PostSensorMeasurementSessions(s *server.Server) httprouter.Handle {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, params)

		if err != nil {
			http.Error(writer, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var sessions []*model.SensorMeasurementSession

		if err := json.NewDecoder(request.Body).Decode(&sessions); err != nil {
			http.Error(writer, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json sessions")
			return
		}

		for _, sensorMeasurementSession := range sessions {
			sensorMeasurementSession.DeviceId = device.ID
		}

		if err := s.Store.CreateSensorMeasurementSessions(sessions); err != nil {
			log.WithField("error", err).Error("could not save sensorMeasurementSession")
			http.Error(writer, "could not save sensorMeasurementSession", 400)
		}
	}
}

func PostMagnetometerData(s *server.Server) httprouter.Handle {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

		device, err := CheckUserDeviceQueryParameters(s.Store, params)

		if err != nil {
			http.Error(writer, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		if request.Body == nil {
			http.Error(writer, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		var magnetometerData []*model.Magnetometer

		if err := json.NewDecoder(request.Body).Decode(&magnetometerData); err != nil {
			http.Error(writer, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json magnetometerData")
			return
		}
		for _, magnetometer := range magnetometerData {
			magnetometer.DeviceId = device.ID
		}

		err = s.Store.CreateMagnetometers(magnetometerData)

		if err != nil {
			log.WithField("error", err).Error("could not save magnetometerData")
			http.Error(writer, "could not save magnetometerData", 500)
		}
	}
}

func PostAccelerometerData(s *server.Server) httprouter.Handle {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, params)

		if err != nil {
			http.Error(writer, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var accelerometerData []*model.Accelerometer

		if request.Body == nil {
			http.Error(writer, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		if err := json.NewDecoder(request.Body).Decode(&accelerometerData); err != nil {
			http.Error(writer, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json accelerometer data")
			return
		}

		for _, accelerometer := range accelerometerData {
			accelerometer.DeviceId = device.ID
		}

		if err := s.Store.CreateAccelerometers(accelerometerData); err != nil {
			log.WithField("error", err).Error("could not save accelerometer")
			http.Error(writer, "could not save accelerometer", 500)
		}
	}
}

func PostGyroscopeData(s *server.Server) httprouter.Handle {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		if request.Body == nil {
			http.Error(writer, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		device, err := CheckUserDeviceQueryParameters(s.Store, params)

		if err != nil {
			http.Error(writer, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var gyroscopeData []*model.Gyroscope

		if err := json.NewDecoder(request.Body).Decode(&gyroscopeData); err != nil {
			http.Error(writer, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json gyroscope data")
			return
		}
		for _, gyroscope := range gyroscopeData {
			gyroscope.DeviceId = device.ID
		}

		if err := s.Store.CreateGyroscopes(gyroscopeData); err != nil {
			log.WithField("error", err).Error("could not save gyroscopeData")
			http.Error(writer, "could not save gyroscopeData", 500)
		}
	}
}
