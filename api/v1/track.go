package v1

import (
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"

	"encoding/json"
	"gitlab.com/tabi/tabi-backend/model/null"
)

func PostTracks(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, ps)

		if err != nil {
			http.Error(w, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
		}

		var tracks []*model.Track

		//check if empty body
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		//check for errors while decoding
		if err := json.NewDecoder(r.Body).Decode(&tracks); err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json tracks")
			return
		}

		for _, track := range tracks {
			track.DeviceId = device.ID
		}

		if err := s.Store.CreateTracks(tracks); err != nil {
			log.WithField("error", err).Error("could not save track")
			http.Error(w, "could not save track", 400)
		}
	}
}

func PostTrackMotives(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, ps)

		if err != nil {
			http.Error(w, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var trackMotives []*model.TrackMotive
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}
		err = json.NewDecoder(r.Body).Decode(&trackMotives)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json trackmotives")
			return
		}

		for _, value := range trackMotives {
			track, err := s.Store.GetTrackByDeviceAndLocalId(device.ID, uint(value.LocalTrackId.Int64))

			if err != nil {
				log.WithField("error", err).Error("GetTrack for trackmotive error store error")
				value.TrackId = null.NullInt64{}
			} else {
				value.TrackId = null.ToNullInt64(int64(track.ID))
			}

			value.DeviceId = device.ID
		}

		err = s.Store.CreateTrackMotives(trackMotives)
		if err != nil {
			http.Error(w, "store error", 500)
			log.WithField("error", err).Error("PostStops store error")
			return
		}
	}
}
