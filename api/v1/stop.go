package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
)

func PostStops(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, ps)

		if err != nil {
			http.Error(w, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var stops []*model.UserStop
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}
		err = json.NewDecoder(r.Body).Decode(&stops)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json batteryinfo")
			return
		}

		for _, value := range stops {
			value.DeviceId = device.ID
		}

		err = s.Store.CreateStops(stops)
		if err != nil {
			http.Error(w, "store error", 500)
			log.WithField("error", err).Error("PostStops store error")
			return
		}
	}
}

func PostStopMotives(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, ps)

		if err != nil {
			http.Error(w, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var stopMotives []*model.UserStopMotive
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}
		err = json.NewDecoder(r.Body).Decode(&stopMotives)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json stopmotives")
			return
		}

		for _, value := range stopMotives {
			stopVisit, err := s.Store.GetStopVisitByDeviceAndLocalId(device.ID, value.LocalStopVisitId)

			if err != nil {
				http.Error(w, "store error", 500)
				log.WithField("error", err).Error("GetStop store error")
				return
			}

			value.DeviceId = device.ID
			value.StopVisitId = null.ToNullInt64(int64(stopVisit.Id))
		}

		err = s.Store.CreateStopMotives(stopMotives)
		if err != nil {
			http.Error(w, "store error", 500)
			log.WithField("error", err).Error("PostStops store error")
			return
		}
	}
}

func PostStopVisits(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, ps)

		if err != nil {
			http.Error(w, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
			return
		}

		var stopVisits []*model.UserStopVisit
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}
		err = json.NewDecoder(r.Body).Decode(&stopVisits)
		if err != nil {
			log.WithFields(log.Fields{"error": err}).Debug("Could not decode to stopsvisits")
			http.Error(w, err.Error(), 400)
			return
		}
		for _, value := range stopVisits {
			value.DeviceId = device.ID

			// Make sure StopId is nil
			value.StopId = null.NullInt64{}
		}

		log.WithFields(log.Fields{"stopVisits": stopVisits, "device": device}).Debug("Post Stop Visits")

		err = s.Store.CreateStopVisits(stopVisits)
		if err != nil {
			http.Error(w, "store error", 500)
			log.WithField("error", err).Error("PostStopVisits store error")
			return
		}
	}
}
