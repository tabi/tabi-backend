package v1

import (
	"github.com/julienschmidt/httprouter"
	"gitlab.com/tabi/tabi-backend/api"
	"gitlab.com/tabi/tabi-backend/server"
)

// swagger:route GET /user users
//
// User endpoint
//
// This will show all available pets by default.
// You can get the pets that are out of stock
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Schemes: http, https
//
//     Security:
//       api_key:
//
//     Responses:
//       default: genericError

func RegisterRoutes(s *server.Server, router *httprouter.Router) {

	availableMiddleware := api.GetMiddleware(s)

	umid := availableMiddleware.UnrestrictedMiddleware
	mid := availableMiddleware.BaseMiddleware
	midUser := availableMiddleware.UserMiddleware
	midAdmin := availableMiddleware.AdminMiddleware

	// Authentication and authorization

	if s.Conf.OpenRegistration {
		router.POST("/api/v1/register", mid.UseHandleAndServe(PostRegister(s)))
	} else {
		router.POST("/api/v1/register", midAdmin.UseHandleAndServe(PostRegister(s)))
	}
	router.POST("/api/v1/token", mid.UseHandleAndServe(PostNewToken(s)))
	router.GET("/api/v1/ping", umid.UseHandleAndServe(GetPing(s)))

	//router.GET("/api/v1/validate_device", server.GetIsDeviceUnauthorized(s))

	// Device

	// swagger:route GET /user/{uid}/device devices getDevices
	router.GET("/api/v1/user/:uid/device", midUser.UseHandleAndServe(GetDevices(s)))

	// swagger:route POST /user/{uid}/device devices addDevice
	router.POST("/api/v1/user/:uid/device", midUser.UseHandleAndServe(PostDevice(s)))

	// swagger:route GET /user/{uid}/device/{did} devices getDevice
	router.GET("/api/v1/user/:uid/device/:did", midUser.UseHandleAndServe(GetDevice(s)))

	router.GET("/api/v1/user/:uid/device/:did/config", midUser.UseHandleAndServe(GetConfig(s)))

	//
	router.GET("/api/v1/user/:uid/device/:did/attribute/:attr", midUser.UseHandleAndServe(GetAttribute(s)))
	router.PUT("/api/v1/user/:uid/device/:did/attribute/:attr", midUser.UseHandleAndServe(PutAttribute(s)))
	//router.DELETE("/api/v1/user/:uid/device/:did/attribute/:attr", midUser.UseHandleAndServe(server.DeleteAttribute(s)))
	//
	//router.GET("/api/v1/user/:uid/device/:did/positionentry", midAdmin.UseHandleAndServe(server.GetPositionEntries(s)))

	// swagger:route GET /user/{uid}/device/{did}/positionentry positions addPositions
	router.POST("/api/v1/user/:uid/device/:did/positionentry", midUser.UseHandleAndServe(PostPositionEntries(s)))

	router.POST("/api/v1/user/:uid/device/:did/question", midUser.UseHandleAndServe(PostQuestions(s)))

	router.POST("/api/v1/user/:uid/device/:did/stops", midUser.UseHandleAndServe(PostStops(s)))

	router.POST("/api/v1/user/:uid/device/:did/stops/motives", midUser.UseHandleAndServe(PostStopMotives(s)))

	router.POST("/api/v1/user/:uid/device/:did/stopvisits", midUser.UseHandleAndServe(PostStopVisits(s)))
	//
	router.GET("/api/v1/user/:uid/device/:did/logs", midAdmin.UseHandleAndServe(GetLogs(s)))
	router.POST("/api/v1/user/:uid/device/:did/logs", midUser.UseHandleAndServe(PostLogs(s)))

	// swagger:route GET /user/{uid}/device/{did}/battery battery getBattery
	router.GET("/api/v1/user/:uid/device/:did/battery", midAdmin.UseHandleAndServe(GetBatteryInfo(s)))
	// swagger:route POST /user/{uid}/device/{did}/battery battery addBattery
	router.POST("/api/v1/user/:uid/device/:did/battery", midUser.UseHandleAndServe(PostBatteryInfo(s)))

	router.POST("/api/v1/user/:uid/device/:did/track", midUser.UseHandleAndServe(PostTracks(s)))
	router.POST("/api/v1/user/:uid/device/:did/track/motives", midUser.UseHandleAndServe(PostTrackMotives(s)))

	router.POST("/api/v1/user/:uid/device/:did/mode", midUser.UseHandleAndServe(PostTransportationModes(s)))

	// Sensor data
	router.POST("/api/v1/user/:uid/device/:did/sensormeasurementsession", midUser.UseHandleAndServe(PostSensorMeasurementSessions(s)))

	router.POST("/api/v1/user/:uid/device/:did/accelerometer", midUser.UseHandleAndServe(PostAccelerometerData(s)))

	router.POST("/api/v1/user/:uid/device/:did/gyroscope", midUser.UseHandleAndServe(PostGyroscopeData(s)))

	router.POST("/api/v1/user/:uid/device/:did/magnetometer", midUser.UseHandleAndServe(PostMagnetometerData(s)))

	router.POST("/api/v1/user/:uid/device/:did/linearacceleration", midUser.UseHandleAndServe(PostLinearAccelerationData(s)))

	router.POST("/api/v1/user/:uid/device/:did/orientation", midUser.UseHandleAndServe(PostOrientationData(s)))

	router.POST("/api/v1/user/:uid/device/:did/gravity", midUser.UseHandleAndServe(PostGravityData(s)))

	router.POST("/api/v1/user/:uid/device/:did/quaternion", midUser.UseHandleAndServe(PostQuaternionData(s)))

	router.GET("/api/v1/report/users", midAdmin.UseHandleAndServe(GetUserReport(s)))

}
