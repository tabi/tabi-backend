package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
	"strconv"
)

type attributeMessage struct {
	Value string
}

func GetAttribute(s *server.Server) httprouter.Handle {
	// swagger:operation GET /user/{uid}/device/{did}/attribute/{attr} attribute getAttribute
	// ---
	// summary: Get attribute
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/empty"

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		attrKey := ps.ByName("attr")

		didStr := ps.ByName("did")
		uidStr := ps.ByName("uid")

		did, err := strconv.Atoi(didStr)
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		uid, err := strconv.Atoi(uidStr)
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		device, err := s.Store.GetDevice(uint(did))
		if err != nil {
			log.WithField("error", err).Error("Could not retrieve device from store")
			http.Error(w, "store error", 500)
			return
		}

		if uint(uid) != device.UserId {
			http.Error(w, "Unauthorized", 400)
			return
		}

		attribute, err := s.Store.GetAttribute(uint(did), attrKey)
		if err != nil {
			log.WithField("error", err).Error("Could not retrieve attribute from store")
			http.Error(w, "store error", 500)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(attribute.Value)
	}
}

func PutAttribute(s *server.Server) httprouter.Handle {
	// swagger:operation PUT /user/{uid}/device/{did}/attribute/{attr} attribute addAttribute
	// ---
	// summary: Add attribute
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/empty"

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}

		attrKey := ps.ByName("attr")

		didStr := ps.ByName("did")
		uidStr := ps.ByName("uid")

		did, err := strconv.Atoi(didStr)
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		uid, err := strconv.Atoi(uidStr)
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		var message string

		err = json.NewDecoder(r.Body).Decode(&message)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		device, err := s.Store.GetDevice(uint(did))

		if uint(uid) != device.UserId {
			http.Error(w, "Unauthorized", 400)
			return
		}

		attribute := model.Attribute{DeviceID: device.ID, Key: attrKey, Value: message}
		err = s.Store.CreateAttribute(&attribute)

		if err != nil {
			log.WithField("error", err).Error("Could not retrieve attribute from store")
			http.Error(w, "store error", 500)
			return
		}
	}
}
