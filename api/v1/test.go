package v1

import "testing"

func checkResponseCode(t *testing.T, expected int, code int) {
	if code == expected {
		if code != expected {
			t.Errorf("Expected response code %d. Got %d\n", expected, code)
		}
	}
}
