package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
	"strconv"
)

func PostDevice(s *server.Server) httprouter.Handle {
	// swagger:operation POST /user/{uid}/device devices addDevice
	// ---
	// summary: Add a device
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/empty"

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		uid, err := strconv.Atoi(ps.ByName("uid"))
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		var device model.Device
		err = json.NewDecoder(r.Body).Decode(&device)
		if err != nil {
			log.WithField("error", err).Error("Could not decode body")
			http.Error(w, "Could not decode body", 400)
			return
		}

		device.UserId = uint(uid)

		if err = s.Store.CreateDevice(&device); err != nil {
			log.WithField("error", err).Error("Could not create device")
			http.Error(w, "Could not store object", 500)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(device)
	}
}

func GetDevices(s *server.Server) httprouter.Handle {
	// swagger:operation GET /user/{uid}/device devices getDevices
	// ---
	// summary: List all devices beloning to a user
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/listDevices"

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		uid, err := strconv.Atoi(ps.ByName("uid"))
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		devices, err := s.Store.GetDevicesByUserId(uint(uid))
		if err != nil {
			log.WithField("error", err).Error("Could not retrieve devices from store")
			http.Error(w, "store error", 500)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(devices)
	}
}

// swagger:operation GET /user/{uid}/device/{did} devices getDevice
// ---
// summary: Get a device
// produces:
// - application/json
// responses:
//   "200":
//     "$ref": "#/responses/getDevice"
func GetDevice(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did, err := strconv.Atoi(ps.ByName("did"))
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		uid, err := strconv.Atoi(ps.ByName("uid"))
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		device, err := s.Store.GetDevice(uint(did))
		if err != nil {
			log.WithField("error", err).Error("Could not retrieve devices from store")
			http.Error(w, "store error", 500)
			return
		}

		if device.UserId != uint(uid) {
			http.Error(w, "Not found", 400)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(device)
	}
}
