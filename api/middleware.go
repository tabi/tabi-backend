package api

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/router/middleware"
	"gitlab.com/tabi/tabi-backend/server"
	"gitlab.com/tabi/tabi-backend/store"
	"gitlab.com/tabi/tabi-backend/util"
	"gitlab.com/vpl/httproutermiddleware"
	"net/http"
)

type MiddlewareSetup struct {
	UnrestrictedMiddleware *httproutermiddleware.Manager
	BaseMiddleware         *httproutermiddleware.Manager
	UserMiddleware         *httproutermiddleware.Manager
	AdminMiddleware        *httproutermiddleware.Manager
}

func unauthorizedSendLog(store store.Store) middleware.UnauthorizedClientFunc {

	return func(r *http.Request, clientId string, clientKey string) {
		e := util.NewEventLog(r, model.Event(model.UnknownClient), "Unauthorized client", "denied")
		store.CreateEvent(&e)
	}

}

func GetMiddleware(s *server.Server) MiddlewareSetup {

	umid := httproutermiddleware.New()
	umid.UseHandlerFunc(middleware.RequestMetricMiddlewareFunc(s.Metrics))
	umid.UseHandlerFunc(middleware.GzipMiddlewareFunc)

	mid := umid

	if s.Conf.ClientAuthentication {
		clientAuth := middleware.NewClientAuthenticationMiddleware(s.Store.GetClient, unauthorizedSendLog(s.Store))
		mid = umid.With(clientAuth)
	}

	// Middleware to check if device belongs to user!
	authorizationMiddleware := middleware.UserResourceAuthorizationMiddleware{Signkey: []byte(s.Conf.SecretKey), UserIdentifier: "uid", AllowedRoles: []model.Role{model.Admin}}
	midUser := mid.With(&authorizationMiddleware).With(middleware.NewUserResourceVerification(s.Store))

	roleMiddleware := middleware.RoleAuthorizationMiddleware{Signkey: []byte(s.Conf.SecretKey), DefaultAllow: false, AllowedRoles: []model.Role{model.Admin}}
	midAdmin := mid.With(&roleMiddleware)

	return MiddlewareSetup{
		UnrestrictedMiddleware: umid,
		BaseMiddleware:         mid,
		UserMiddleware:         midUser,
		AdminMiddleware:        midAdmin,
	}
}
