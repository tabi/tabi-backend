package responses

import (
	"gitlab.com/tabi/tabi-backend/model"
)

// swagger:parameters addBattery
type AddBatterySwaggerParameters struct {
	// in: path
	Uid int `json:"uid"`

	// in: path
	Did int `json:"did"`

	// in: body
	// required: true
	Body model.BatteriesInfo
}

// swagger:parameters getBattery
type GetBatterySwaggerParameters struct {
	// in: path
	Uid int `json:"uid"`

	// in: path
	Did int `json:"did"`
}

// ListDeviceResponse returns all devices belonging to a user
// swagger:response getBattery
type BatteryInfoList struct {
	// in: body
	Body []model.BatteryInfo
}
