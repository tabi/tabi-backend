package responses

// swagger:parameters addAttribute
type AddAttributeParameters struct {
	// User identifier
	// in: path
	Uid int `json:"uid"`

	// Device identifier
	// in: path
	Did int `json:"did"`

	// Attribute key
	// in: path
	Attr string `json:"attr"`

	// Attribute value
	// in: body
	Body string
}

// swagger:parameters getAttribute
type GetAttributeParameters struct {
	// User identifier
	// in: path
	Uid int `json:"uid"`

	// Device identifier
	// in: path
	Did int `json:"did"`

	// Attribute key
	// in: path
	Attr string `json:"attr"`
}
