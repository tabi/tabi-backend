package responses

import "gitlab.com/tabi/tabi-backend/model"

// swagger:parameters addPositions
type AddPositionSwaggerParameters struct {
	// in: path
	Uid int `json:"uid"`

	// in: path
	Did int `json:"did"`

	// in: body
	// required: true
	Body []model.PositionEntry
}

// swagger:parameters getPositions
type GetPositionSwaggerParameters struct {
	// in: path
	Uid int `json:"uid"`

	// in: path
	Did int `json:"did"`
}

// ListDeviceResponse returns all devices belonging to a user
// swagger:response getPositions
type PositionResponseList struct {
	// in: body
	Body []model.PositionEntry
}
