package messages

// swagger:model
type UserMessage struct {
	// the username of this user
	//
	// required: true
	Username string

	// the first name of this user
	//
	// required: false
	FirstName string

	// the last name of this user
	//
	// required: false
	LastName string

	// the password of this user
	//
	// required: true
	Password string

	// the email of this user
	//
	// required: false
	Email string
}

// swagger:model
type UserLoginMessage struct {
	// the username of this user
	//
	// required: true
	Username string

	// the password of this user
	//
	// required: true
	Password string
}

type RegisterUserMessage struct {
	Username string
	Email    string
	Password string
}

// TokenResult represents a JWT
//
//
// swagger:model
type TokenResult struct {
	UserId uint
	Token  string
}
