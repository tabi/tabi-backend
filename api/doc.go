//go:generate swagger generate spec -o ../public/swagger.json -b ../cmd/tabi-backend

// Package api Tabi API
//
// This swagger file documents the API of the tabi-backend.
//
// Still a work in progress
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http, https
//     Host: localhost:8000
//     BasePath: /api/v1
//     Version: 0.0.1
//     License: MIT https://gitlab.com/tabi/tabi-backend/blob/master/LICENSE
//     Contact: Victor Verstappen<vpl@vpl.me> http://vpl.me
//
//     Security:
//     - api_key:
//     - client_id:
//     - client_key:
//
//     SecurityDefinitions:
//     api_key:
//          type: apiKey
//          name: Authorization
//          in: header
//     client_id:
//          type: apiKey
//          name: X-Tabi-Client-Identifier
//          in: header
//     client_key:
//          type: apiKey
//          name: X-Tabi-Client-Key
//          in: header
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
package api
