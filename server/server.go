package server

import (
	"crypto/tls"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rifflock/lfshook"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/config"
	"gitlab.com/tabi/tabi-backend/store"
	"gitlab.com/tabi/tabi-backend/store/datastore/sql"
	"golang.org/x/crypto/acme/autocert"
	"net/http"
	"os"
)

type GormLogger struct{}

func (*GormLogger) Print(v ...interface{}) {
	if v[0] == "sql" {
		log.WithFields(log.Fields{"module": "gorm", "type": "sql"}).Debug(v[3])
	}
	if v[0] == "log" {
		log.WithFields(log.Fields{"module": "gorm", "type": "log"}).Debug(v[2])
	}
}

type Server struct {
	Store   store.Store
	Conf    *config.Config
	Metrics Metrics
}

type Metrics struct {
	HttpRequestsTotal    *prometheus.CounterVec
	HttpRequestsDuration prometheus.Summary
	Logins               *prometheus.CounterVec
	PostgresTx           *prometheus.CounterVec
}

func setupMetrics() Metrics {
	metrics := Metrics{}

	metrics.HttpRequestsTotal = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "tabi_http_requests_total",
		Help: "How many total http requests have been handled"},
		[]string{"status"})
	prometheus.MustRegister(metrics.HttpRequestsTotal)

	metrics.Logins = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "tabi_logins_total",
		Help: "How many logins have been handled"},
		[]string{"status"})
	prometheus.MustRegister(metrics.Logins)

	metrics.HttpRequestsDuration = prometheus.NewSummary(
		prometheus.SummaryOpts{
			Name:       "tabi_request_durations",
			Help:       "Tabi requests latencies in seconds",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		})
	prometheus.MustRegister(metrics.HttpRequestsDuration)

	return metrics
}

func getServer(host string, config config.LetsEncryptConfig) *http.Server {
	if !config.Enabled || !config.AcceptTOS {
		return &http.Server{
			Addr: host}
	}

	m := autocert.Manager{
		Cache:      autocert.DirCache("certs"),
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist(config.Domain),
	}
	server := &http.Server{
		Addr:      ":https",
		TLSConfig: &tls.Config{GetCertificate: m.GetCertificate},
	}

	log.Info("Enabling LetsEncrypt")

	return server
}

func NewDefaultServer() (*Server, *http.Server) {
	return NewServer("server.toml")
}

func ConnectPostgres(postgresConfig config.PostgresConfig) (*sqlx.DB, error) {
	if postgresConfig.Port == 0 {
		postgresConfig.Port = 5432
	}

	args := fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable password=%s",
		postgresConfig.Host, postgresConfig.Port, postgresConfig.User, postgresConfig.Database, postgresConfig.Password)
	db, err := sqlx.Connect("postgres", args)

	return db, err

}

func SetupConfigAndStore(configPath string) (config.Config, store.Store) {
	conf, err := config.LoadTomlFile(configPath)
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Fatal("Could not load configuration")
	}

	db, err := ConnectPostgres(conf.Postgres)

	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Fatal("Could not establish database connection")
	}

	store := sql.NewDatastore(db)

	return conf, store
}

func NewServer(configPath string) (*Server, *http.Server) {
	conf, store := SetupConfigAndStore(configPath)

	setupLogging(&conf)

	metrics := setupMetrics()
	return &Server{
		store, &conf, metrics,
	}, getServer(conf.Host, conf.LetsEncrypt)
}

func setupLogging(conf *config.Config) {
	log.AddHook(lfshook.NewHook(lfshook.PathMap{
		log.InfoLevel:  "info.log",
		log.ErrorLevel: "error.log",
	}, &log.JSONFormatter{}))

	lvl, err := log.ParseLevel(string(conf.LogLevel))
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Fatal("Could not parse loglevel string")
	}
	log.SetLevel(lvl)

	log.SetOutput(os.Stdout)
}

func (s *Server) Close() {
	s.Store.Close()
}
